# AdvancedGreetingLightRest

Spring REST API with Spring Security Integration (customized configuration)

## Overall
This application shows how to implement Spring Security with custom configuration<br>
Initially, boilerplate has been taken from GreetingLightRest application,
which can be found at https://bitbucket.org/reborn0105/greetinglightrest
Business layer designed so every Account can have multiple roles

## Project contents:
  * Spring Boot - Base of application that maintain application runtime
  * Spring Data - Module that allows to persist (save objects) to database (HSQLDB in this scope)
  * HSQLDB Database - Database that performs serialization/deserialization during in-memory execution
  * Spring Security - Framework that allows to secure Java applications

## Project requirements: 
  * Java 8 installed
  * Maven 3.0 (3.9 recommended)
  * Any IDE (optional)

## User credentials:
  * Username: user Password: password
  * Username: operations Password: operations
  
## How to launch?
```
git clone https://reborn0105@bitbucket.org/reborn0105/advancedgreetinglightrest.git
cd ./advancedgreetinglightrest
mvn spring-boot:run
```

Once application started visit http://localhost:8080/api/greetings to see results